# QuickSheet

## What is it?
Quicksheet is a UIViewController wrapper that handles bottom sheet presentation with low code footprint and it's highly customizable.

## Why should you use it?
Unfortunately, the native solution for bottom sheet presentation is only available for iOS 15 with limited functionality that's expanded upon in iOS 16. So, the only option for apps that require wider iOS support is to rely on third-party libraries to handle this intraction but it could require adding a lot of additional code in addition to the need to restructure how you build your view controllers which might make it difficult to transition to the native solution when it's suitable for your app, especially in large scale applications.

[![CI Status](https://img.shields.io/travis/Ahmed Fathy/QuickSheet.svg?style=flat)](https://travis-ci.org/Ahmed Fathy/QuickSheet)
[![Version](https://img.shields.io/cocoapods/v/QuickSheet.svg?style=flat)](https://cocoapods.org/pods/QuickSheet)
[![License](https://img.shields.io/cocoapods/l/QuickSheet.svg?style=flat)](https://cocoapods.org/pods/QuickSheet)
[![Platform](https://img.shields.io/cocoapods/p/QuickSheet.svg?style=flat)](https://cocoapods.org/pods/QuickSheet)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

QuickSheet is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'QuickSheet'
```

## Author

Ahmed Fathy, ahmedfathy.mha@gmail.com

## License

QuickSheet is available under the MIT license. See the LICENSE file for more info.
