//
//  ViewController.swift
//  QuickSheet
//
//  Created by Ahmed Fathy on 01/08/2023.
//  Copyright (c) 2023 Ahmed Fathy. All rights reserved.
//

import UIKit
import QuickSheet

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didPressPresent(_ sender: UIButton) {
        let shadowStyle = QuickSheetOptions.ShadowStyle(shadowRadius: Preferences.values[.shadowRadius] as! CGFloat,
                                                        shadowColor: .black,
                                                        shadowOpacity: Preferences.values[.shadowOpacity] as! Float)
        let options = QuickSheetOptions(fraction: Preferences.values[.fraction] as! Double,
                                        isExpandable: Preferences.values[.expandable] as! Bool,
                                        isScrollable: Preferences.values[.scrollable] as! Bool,
                                        cornerRadius: Preferences.values[.cornerRadius] as! CGFloat,
                                        blurEffect: Preferences.values[.blurEffect] as! UIBlurEffect.Style,
                                        shadowStyle: shadowStyle)
        let dummy = storyboard!.instantiateViewController(withIdentifier: "DummyVC")
        self.presentQuickSheet(dummy, options: options)
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Preferences.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PreferenceCell
        cell.key = Preferences.allCases[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

